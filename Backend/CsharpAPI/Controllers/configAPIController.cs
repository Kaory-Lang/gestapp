using System;
using System.Data.SqlClient;
using Controllers.connectionToSQLServer;
using System.Text.Json;

namespace Controllers.configAPIController;

class ConfigAPIController {
	public static bool addWarehouse(string warehouseUbication) {
		if(warehouseUbication == "") return false;

		try {
			SqlConnection connection = ConnectionToSQLServer.doConnection();
			string query = $"INSERT INTO Warehouse VALUES ('{warehouseUbication}')";

			SqlCommand command = new SqlCommand(query, connection);
			command.ExecuteNonQuery();
			Console.WriteLine("Query done");
			Console.WriteLine("----------");
			connection.Close();
			return true;

		}catch(SqlException e){
			return false;
		}
	} 

	public static bool addRack(int whseNumber, int HeightCms, int XWidthCms, int YWidthCms) {
		int freeSpace = HeightCms * XWidthCms * YWidthCms;

		try {
			SqlConnection connection = ConnectionToSQLServer.doConnection();
			string query = $"INSERT INTO Racks VALUES ({whseNumber}, {HeightCms}, {XWidthCms}, {YWidthCms}, {freeSpace})";

			SqlCommand command = new SqlCommand(query, connection);
			command.ExecuteNonQuery();
			Console.WriteLine("Query done");
			Console.WriteLine("----------");
			connection.Close();
			return true;

		}catch (SqlException e) {
			return false;
		}
	}
	
	public static string listWarehouse() {
		SqlConnection connection = ConnectionToSQLServer.doConnection();

		string query = $"SELECT * FROM Warehouse";

		SqlCommand command = new SqlCommand(query, connection);
		SqlDataReader reader = command.ExecuteReader();

		List<object> list = new List<object>();

		while(reader.Read()) {
			object warehouse = new {
				id = reader.GetInt32(0),
				ubication = reader.GetString(1)
			};

			list.Add(warehouse);
		}
		Console.WriteLine("Query done");
		Console.WriteLine("----------");

		return JsonSerializer.Serialize(list);
	}

	public static string listRacks(int whseId) {
		SqlConnection connection = ConnectionToSQLServer.doConnection();

		string query = $"SELECT * FROM Racks WHERE Rack_Warehouse = {whseId}";

		SqlCommand command = new SqlCommand(query, connection);
		SqlDataReader reader = command.ExecuteReader();

		List<object> list = new List<object>();

		while(reader.Read()) {
			object rack = new {
				id = reader.GetInt32(0),
				heightCms = reader.GetInt32(2),
				XWidthCms = reader.GetInt32(3),
				YWidthCms = reader.GetInt32(4),
				freeSpace = reader.GetInt32(5)
			};

			list.Add(rack);
		}
		Console.WriteLine("Query done");
		Console.WriteLine("----------");

		return JsonSerializer.Serialize(list);

	}
}

