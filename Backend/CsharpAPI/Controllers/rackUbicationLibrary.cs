using System.Data.SqlClient;
using Controllers.connectionToSQLServer;
using System.Text.Json;

namespace Controllers.rackUbicationLibrary;

class RackUbicationLibrary {
	public static int getUbication(long volume) {
		SqlConnection connection = ConnectionToSQLServer.doConnection();
		int rackID = -1;
		int freeSpace = 0;

		string query;
		query = "SELECT * FROM RACKS";

		SqlCommand command = new SqlCommand(query, connection);
		SqlDataReader reader = command.ExecuteReader();

		while(reader.Read()) {
			if(volume < reader.GetInt32(5)) {
				rackID = reader.GetInt32(0);
				freeSpace = reader.GetInt32(5);
				break;
			}
		}

		reader.Close();

		if(rackID != -1) {
			Console.WriteLine("Space = " + (freeSpace - volume));
			Console.WriteLine("Rack = " + rackID);
			query = $"UPDATE Racks SET Rack_FreeSpace = {freeSpace - volume} WHERE Rack_ID = {rackID}";
			command = new SqlCommand(query, connection);
			command.ExecuteNonQuery();
			connection.Close();
		}

		return rackID;
	}

	public static string showUbication(int packagingId) {
		SqlConnection connection = ConnectionToSQLServer.doConnection();
		SqlCommand command;
		SqlDataReader reader;
		string query;

		int rackID, warehouseId;
		string warehouseName;

		if(packagingId == -1) {
			query = $"SELECT TOP 1 Pack_Ubication FROM Packaging ORDER BY Pack_ID DESC";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();

			reader.Read();
			rackID = reader.GetInt32(0);
			reader.Close();

			query = $"SELECT Rack_Warehouse FROM Racks WHERE Rack_ID = {rackID}";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();
			reader.Read();
			warehouseId = reader.GetInt32(0);
			reader.Close();

			query = $"SELECT Whse_Ubication FROM Warehouse WHERE Whse_ID = {warehouseId}";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();
			reader.Read();
			warehouseName = reader.GetString(0);
			reader.Close();

			object obj = new {
				warehouse = warehouseName,
				warehouseId = warehouseId,
				rack = rackID
			};

			return JsonSerializer.Serialize(obj);
		}else {
			query = $"SELECT Pack_Ubication FROM Packaging WHERE Pack_ID = {packagingId}";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();

			reader.Read();
			rackID = reader.GetInt32(0);
			reader.Close();

			query = $"SELECT Rack_Warehouse FROM Racks WHERE Rack_ID = {rackID}";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();
			reader.Read();
			warehouseId = reader.GetInt32(0);
			reader.Close();

			query = $"SELECT Whse_Ubication FROM Warehouse WHERE Whse_ID = {warehouseId}";
			command = new SqlCommand(query, connection);
			reader = command.ExecuteReader();
			reader.Read();
			warehouseName = reader.GetString(0);
			reader.Close();

			object obj = new {
				warehouse = warehouseName,
				warehouseId = warehouseId,
				rack = rackID
			};

			return JsonSerializer.Serialize(obj);
		}
		connection.Close();
	}
}
