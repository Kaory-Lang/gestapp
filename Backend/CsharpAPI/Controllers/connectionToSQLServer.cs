using System;
using System.Data.SqlClient;

namespace Controllers.connectionToSQLServer;

class ConnectionToSQLServer {
	public static SqlConnection doConnection() {
		try {
			//Intancies and config DB credentials
			SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
			builder.DataSource = "localhost";
			builder.UserID = "SA";
			builder.Password = "Test1234";
			builder.InitialCatalog = "GestAppDB";

			//Connecting
			Console.WriteLine("Connecting to SQL Server");
			
			SqlConnection connection = new SqlConnection(builder.ConnectionString);
			connection.Open();
			
			Console.WriteLine("Successful");

			return connection;
		} catch (SqlException e) {
			Console.WriteLine(e.ToString());
			return null;
		}
	}
}
