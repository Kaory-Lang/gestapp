using System;
using System.Data.SqlClient;
using Controllers.connectionToSQLServer;

namespace Controllers.usersAPIController;

class UsersAPIController {
	public static bool validate_credentials(string userInput, string passwordInput) {
		try {
			SqlConnection connection = ConnectionToSQLServer.doConnection();
			string query = $"SELECT COUNT(*) FROM Users WHERE User_Name = {userInput} and User_Password = {passwordInput}";

			SqlCommand command = new SqlCommand(query, connection);
			SqlDataReader reader = command.ExecuteReader();
			Console.WriteLine("Query done");

			reader.Read();
			if(reader.GetInt32(0) >= 1) {
				connection.Close();
				Console.WriteLine("Connection closed");
				Console.WriteLine("-----------------");
				return true;
			}
		}
		catch (SqlException e) {
			Console.WriteLine(e.ToString());
		}
		
		return false;
	}
}
