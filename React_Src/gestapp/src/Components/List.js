import '../Styles/Components_Styles/list_component_style.css';

export default function List(props) {
	const list = [];

	let values;

	if(props.messageTemplate === "test") values = [1,2,3,4,5,6,8,9,0,9];
	else values = props.jsonData;

	let btnAction = () => {};
	if(props.btnAction !== undefined) btnAction = props.btnAction;

	let cfgStyles = '';
	if(props.messageTemplate === "logs") cfgStyles = "cft-1";
	else if(props.messageTemplate === "warehouse") cfgStyles = "cft-2";
	else if(props.messageTemplate === "rack") cfgStyles = "cft-3";

	if(values !== undefined) {
		values.forEach((curr, index) => {
			list.push(
				<div id="list-item-container" class={cfgStyles} onClick={btnAction}>

					{
						(props.messageTemplate === "warehouse")
						? <div class="list-item-label" data-id={curr.id}>
							<p>Warehouse #{curr.id}</p>
							<p>Ubication: {curr.ubication}</p>
						  </div>
						: (props.messageTemplate === "rack") 
						? <div class="list-item-label">
							<p>Rack #{curr.id}</p>
							<p>Height: {curr.heightCms}</p>
							<p>Front Size: {curr.XWidthCms}</p>
							<p>Side Size: {curr.YWidthCms}</p>
							<p>Free Space: {curr.freeSpace}</p>
						  </div>
						: (props.messageTemplate === "logs") 
						? <div class="list-item-label">
							<p>Transaction #{curr.invId} Date: {curr.invDate}</p>
							<p>Packaging ID: {curr.invPackaging}</p>
							{(!curr.invOperation2) 
							? <><p>Client ID: {curr.invCli}</p> <p>Operation: Out</p></>
							: <><p>Provider ID: {curr.invProv}</p> <p>Operation: In</p></>}
							<p>Date: {curr.invDate}</p>
						  </div>
						: <h1 class="list-item-label">{index}</h1>
					}

					<p class="list-details-btn">DETAILS</p>
				</div>
			);
		});
	}

	return list;
}
