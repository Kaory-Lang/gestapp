import React from 'react';
import '../Styles/transaction_register_styles.css';
import BackBtn from '../Components/BackBtn';
import Menu from './Menu';
import url from '../Components/APIUrl';

class Transaction_Register extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			selectorValue: true,
			warehouseUbication: 'warehouse #1',
			goMenu: false,
			category: '',
			errorMessage: "",
			ubicationLabel: ""
		};

		this.handlerSelector = this.handlerSelector.bind(this);
		this.registerBtnHandler = this.registerBtnHandler.bind(this);
		this.chgCategoryHandler = this.chgCategoryHandler.bind(this);
		
		this.postRegistration = this.postRegistration.bind(this);
	}
	
	handlerSelector() {
		this.setState({
			selectorValue: !this.state.selectorValue,
			category: '',
			ubicationLabel: ''
		});
	}

	chgCategoryHandler(e) {
		const divs = document.getElementById("transactionReg-screen-category-subcontainer").childNodes;
		divs.forEach(curr => {
			curr.style = "background-color: initial;";
		});

		if(e.target.dataset.cat !== undefined) e.target.style = "background-color: #307FE2;";

		this.setState({category: e.target.dataset.cat});
	}

	postRegistration(errNumber, packagingNumber) {
		// Errors deal
		if(errNumber === 0) this.setState({errorMessage: "Successful"});
		else if(errNumber === 2) this.setState({errorMessage: "Packaging name can't be null"});
		else if(errNumber === 3) this.setState({errorMessage: "Inexistent packaging ID"});
		else if(errNumber === 4) this.setState({errorMessage: "Price can't be null"});
		else if(errNumber === 5) this.setState({errorMessage: "Invalid Provider ID"});
		else if(errNumber === 6) this.setState({errorMessage: "Invalid Client ID"});
		else if(errNumber === 7) this.setState({errorMessage: "Invalid size"});
		else if(errNumber === 8) this.setState({errorMessage: "Select a category"});
		else if(errNumber === 9) this.setState({errorMessage: "Quantity can't be null"});
		else if(errNumber === 10) this.setState({errorMessage: "Inexistent identification card"});
		else if(errNumber === 11) this.setState({errorMessage: "No space avalible in warehouse"});

		if(errNumber === 0) {
		// Show ubication in bottom label 
		fetch(url + `ubication?packagingId=${packagingNumber ?? -1}`)
			.then(response => response.json())
			.then(data => {
				this.setState({ubicationLabel: <><p>Warehouse: {data.warehouse} (#{data.warehouseId})</p> <p>Rack #{data.rack}</p></>});
			});
		}
	}

	registerBtnHandler(e) {
		e.preventDefault();

		const inputs = document.getElementsByTagName("input");

		if(this.state.selectorValue) {
			fetch(url + `register
						?operation=${this.state.selectorValue}
						&packagingName=${inputs[0].value}
						&quantity=${inputs[1].value || 0}
						&price=${inputs[2].value || 0}
						&providerID=${inputs[3].value}
						&packagingSize=${inputs[4].value}-${inputs[6].value}-${inputs[8].value}
						&category=${this.state.category}&`.replaceAll('\n', '').replaceAll('\t', ''))
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.postRegistration(data);
			})
			.catch(() => this.setState({errorMessage: "ERROR!!! An Error has ocurred registering the data, try again."}));
		}else {
			fetch(url + `register
						?operation=${this.state.selectorValue}
						&packagingName=${inputs[0].value}
						&cliID=${inputs[1].value}
						&quantity=${inputs[2].value || 0}
						&price=0`.replaceAll('\n', '').replaceAll('\t', ''))
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.postRegistration(data, inputs[0].value);
			})
			.catch(() => this.setState({errorMessage: "ERROR!!! An Error has ocurred registering the data, try again."}));;
		}
	}

    render() {
		if(this.state.goMenu) return <Menu />;

        return (
            <div id="transaction-register-screen-container">
				<BackBtn target={this} />

				<h1 id="transaction-register-title">TRANSACTION REGISTER</h1>

				<div id="transaction-register-content">
					<div id="transaction-register-column-n1"></div>

					<div id="transaction-register-column-n2">
						<form id="transaction-register-screen-form">
							<select id="transactionReg-screen-selector" onChange={this.handlerSelector}>
								<option>In</option>
								<option>Out</option>
							</select>

                    		<input className="transaction-register-screen-input" type="text"
							placeholder={(this.state.selectorValue)
							? "Packaging Name" 
							: "Packaging ID"} />

                    		<input className="transaction-register-screen-input" type="text" 
							placeholder={(this.state.selectorValue)
							? "Quantity"
							: "Client ID"} />

							<input className="transaction-register-screen-input" type="text"
							placeholder={(this.state.selectorValue)
							? "Price"
							: "Quantity"} />

							{(this.state.selectorValue) ?
							<>
								<input className="transaction-register-screen-input"
								type="text" placeholder="ProviderID"/>

								<div id="size-input">
									<input class="transaction-register-screen-input-size"
									type="text" placeholder="XWidth"/>

									<input id="transaction-register-screen-input-x-symbol"
									type="text" placeholder="X" readOnly='X' />

									<input class="transaction-register-screen-input-size"
									type="text" placeholder="YWidth"/>

									<input id="transaction-register-screen-input-x-symbol"
									type="text" placeholder="X" readOnly='X' />

									<input class="transaction-register-screen-input-size"
									type="text" placeholder="Height"/>
								</div>
							
								<div id="transactionReg-screen-category-container">
									<h4 id="select-category-label">SELECT CATEGORY OF THE PRODUCT</h4>

									<div id="transactionReg-screen-category-subcontainer" onClick={this.chgCategoryHandler}>
										<div data-cat='G'><span>Generic</span></div>
										<div data-cat='F'><span>Fragile</span></div>
										<div data-cat='Q'><span>Quimic</span></div>
										<div data-cat='f'><span>Food</span></div>
									</div>
								</div>
							</>
							: null}

							<div id="transactionReg-screen-submit-buttons">
								<button id="transactionReg-reg-btn" onClick={this.registerBtnHandler}>Register</button>
								<button id="transactionReg-cancel-btn">Cancel</button>
							</div>

							<div id="transactionReg-screen-ubication-tag">
								<div id="ubication-tag-label">{this.state.ubicationLabel}</div>
								<div id="ubication-tag-see-all">see all</div>
							</div>
                		</form>
					</div>

					<div id="transaction-register-column-n3">
						<p>{this.state.errorMessage}</p>
					</div>
				</div>
            </div>
        )
    }
}

export default Transaction_Register;
