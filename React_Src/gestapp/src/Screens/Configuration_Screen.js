import React from 'react';
import '../Styles/configuration_screen_styles.css';
import List from '../Components/List';
import BackBtn from '../Components/BackBtn';
import Menu from './Menu';
import url from '../Components/APIUrl';

class Configuration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            goMenu: false,
            stateMessage: "",
            warehouseList: undefined, 
            rackContainerContent: "Empty",
			lastTarget: null
        };

        this.addWarehouseHandler = this.addWarehouseHandler.bind(this);
        this.addRackHandler = this.addRackHandler.bind(this);

        this.addWhseBtnHandler = this.addWhseBtnHandler.bind(this);
        this.addRackBtnHandler = this.addRackBtnHandler.bind(this);
        this.detailsBtnHandler = this.detailsBtnHandler.bind(this);
        this.detailsBtnHandlerRack = this.detailsBtnHandlerRack.bind(this);

        fetch(url + `config/listWarehouse`)
        .then(response => response.json())
        .then(data => {
            this.setState({warehouseList: data});
        });
    }

    addWarehouseHandler() {
        document.getElementById("rack-float-window").style.visibility = "hidden";
        document.getElementById("warehouse-float-window").style.visibility = "visible";
        for(let curr of document.getElementsByClassName("state-message")) curr.style.visibility = "hidden";
    }

    addRackHandler() {
        document.getElementById("warehouse-float-window").style.visibility = "hidden";
        document.getElementById("rack-float-window").style.visibility = "visible";
        for(let curr of document.getElementsByClassName("state-message")) curr.style.visibility = "hidden";
    }

    addWhseBtnHandler() {
        const ubication = document.getElementsByClassName("floating-inputs")[0].firstChild.value;

        fetch(url + `config/addWarehouse?warehouseUbication=${ubication}`)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(!data) {
                this.setState({stateMessage: "Error Creating the Warehouse"});
            }else document.getElementById("warehouse-float-window").style.visibility = "hidden";
        });

        document.getElementsByClassName("floating-inputs")[0].childNodes[0].value = "";
    }

    addRackBtnHandler() {
        const inputs = document.getElementsByClassName("floating-inputs")[1].childNodes;

        fetch(url + `config/addRack?whseNumber=${inputs[0].value}&HeightCms=${inputs[1].value}&XWidthCms=${inputs[2].value}&YWidthCms=${inputs[3].value}`)
        .then(response => response.json())
        .then(data => {
            if(!data) {
                console.log(data);
                this.setState({stateMessage: "Error registering rack, maybe 'warehouse number' doesn't exist"});
                document.getElementsByClassName("state-message")[1].style.visibility = "visible";
            }else document.getElementById("rack-float-window").style.visibility = "hidden";
        });
    }

	detailsBtnHandlerRack(e) {
        if(this.state.lastTarget) this.state.lastTarget.style.minHeight = "30px";
        this.setState({lastTarget: e.target});
        e.target.style.minHeight = "175px";
    }

    detailsBtnHandler(e) {
        fetch(url + `config/listRacks?whseId=${e.target.firstChild.dataset.id}`)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            this.setState({rackContainerContent: <List jsonData={data} messageTemplate="rack" btnAction={this.detailsBtnHandlerRack} />});
        })
        document.getElementById("details-float-window").style.visibility = "visible";
    }

    render() {
        if(this.state.goMenu) return <Menu />;

        return (
            <div id="configuration-screen-container">
                <BackBtn target={this} />

                <h1>CONFIGURATION</h1>

                <div id="configuration-screen-subcontainer-btns">
                    <div>
                        <h1 onClick={this.addWarehouseHandler}>Add New Warehouse</h1>
                     </div>
                    <div>
                        <h1 onClick={this.addRackHandler}>Add New Rack</h1>
                    </div>
                </div>

                <div id="configuration-screen-subcontainer">
					<div id="warehouse-list-scroll">
                    	<List jsonData={this.state.warehouseList} messageTemplate="warehouse" btnAction={this.detailsBtnHandler} />
					</div>
                </div>

                <div id="warehouse-float-window" class="float-window">
                    <h1 onClick={() => {
                        document.getElementById("warehouse-float-window").style.visibility = "hidden";
                        for(let curr of document.getElementsByClassName("state-message")) curr.style.visibility = "hidden";
                    }}>X</h1>

                    <div class="floating-inputs">
                        <input type="text" placeholder="Ubication" />
                        <input type="button" value="Create" onClick={this.addWhseBtnHandler} />
                        <p class="state-message">{this.state.stateMessage}</p>
                    </div>
                </div>

                <div id="rack-float-window" class="float-window">
                    <h1 onClick={() => {
                        document.getElementById("rack-float-window").style.visibility = "hidden";
                        for(let curr of document.getElementsByClassName("state-message")) curr.style.visibility = "hidden";
                    }}>X</h1>

                    <div class="floating-inputs">
                        <input type="text" placeholder="Warehouse Number" />
                        <input type="text" placeholder="Heigth Cms" />
                        <input type="text" placeholder="Front width Cms" />
                        <input type="text" placeholder="Side width Cms" />
                        <input type="button" value="Create" onClick={this.addRackBtnHandler} />
                        <p class="state-message">{this.state.stateMessage}</p>
                    </div>
                </div>

                <div id="details-float-window" class="float-window">
                    <h1 onClick={() => {
                        document.getElementById("details-float-window").style.visibility = "hidden";
                        for(let curr of document.getElementsByClassName("state-message")) curr.style.visibility = "hidden";
                    }}>X</h1>

                    <div id="rack-list-container">
                        {this.state.rackContainerContent}
                    </div>

                    <div class="floating-inputs">
                        
                    </div>
                </div>
            </div>
        )
    }
}

export default Configuration;
